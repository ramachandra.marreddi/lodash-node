var baseAt = require('../internal/baseAt'),
    baseFlatten = require('../internal/baseFlatten'),
    restParam = require('../function/restParam');

/**
 * Creates an array of values corresponding to `paths` of `object`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to iterate over.
 * @param {...(string|string[])} [paths] The property paths of elements to pick,
 *  specified individually or in arrays.
 * @returns {Array} Returns the new array of picked elements.
 * @example
 *
 * var object = { 'a': [{ 'b': { 'c': 3 } }, 4] };
 *
 * _.at(object, ['a[0].b.c', 'a[1]']);
 * // => [3, 4]
 *
 * _.at(['a', 'b', 'c'], 0, 2);
 * // => ['a', 'c']
 */
var at = restParam(function(object, paths) {
  return baseAt(object, baseFlatten(paths));
});

module.exports = at;
