var baseDifference = require('../internal/baseDifference'),
    baseFlatten = require('../internal/baseFlatten'),
    isArrayLikeObject = require('../lang/isArrayLikeObject'),
    restParam = require('../function/restParam');

/**
 * Creates an array of unique `array` values not included in the other
 * provided arrays using [`SameValueZero`](http://ecma-international.org/ecma-262/6.0/#sec-samevaluezero)
 * for equality comparisons.
 *
 * @static
 * @memberOf _
 * @category Array
 * @param {Array} array The array to inspect.
 * @param {...Array} [values] The values to exclude.
 * @returns {Array} Returns the new array of filtered values.
 * @example
 *
 * _.difference([3, 2, 1], [4, 2]);
 * // => [3, 1]
 */
var difference = restParam(function(array, values) {
  return isArrayLikeObject(array)
    ? baseDifference(array, baseFlatten(values, false, true))
    : [];
});

module.exports = difference;
