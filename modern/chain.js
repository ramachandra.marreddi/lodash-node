module.exports = {
  'chain': require('./chain/chain'),
  'commit': require('./chain/commit'),
  'concat': require('./chain/concat'),
  'lodash': require('./chain/lodash'),
  'next': require('./chain/next'),
  'plant': require('./chain/plant'),
  'reverse': require('./chain/reverse'),
  'run': require('./chain/run'),
  'tap': require('./chain/tap'),
  'thru': require('./chain/thru'),
  'toIterator': require('./chain/toIterator'),
  'toJSON': require('./chain/toJSON'),
  'value': require('./chain/value'),
  'valueOf': require('./chain/valueOf'),
  'wrapperChain': require('./chain/wrapperChain')
};
