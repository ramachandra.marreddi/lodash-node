var stringToArray = require('../internal/stringToArray'),
    toString = require('../lang/toString');

/** Used to compose unicode character classes. */
var rsAstralRange = '\\ud800-\\udfff',
    rsComboRange = '\\u0300-\\u036f\\ufe20-\\ufe23',
    rsVarRange = '\\ufe0e\\ufe0f';

/** Used to compose unicode capture groups. */
var rsZWJ = '\\u200d';

/** Used to detect strings with [zero-width joiners or code points from the astral planes](http://eev.ee/blog/2015/09/12/dark-corners-of-unicode/). */
var reHasComplexSymbol = RegExp('[' + rsZWJ + rsAstralRange + rsComboRange + rsVarRange + ']');

/**
 * Capitalizes the first character of `string`.
 *
 * @static
 * @memberOf _
 * @category String
 * @param {string} [string=''] The string to capitalize.
 * @returns {string} Returns the capitalized string.
 * @example
 *
 * _.capitalize('fred');
 * // => 'Fred'
 */
function capitalize(string) {
  string = toString(string);
  if (!string) {
    return string;
  }
  if (reHasComplexSymbol.test(string)) {
    var strSymbols = stringToArray(string);
    return strSymbols[0].toUpperCase() + strSymbols.slice(1).join('');
  }
  return string.charAt(0).toUpperCase() + string.slice(1);
}

module.exports = capitalize;
