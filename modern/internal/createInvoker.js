var arrayMap = require('./arrayMap'),
    baseFlatten = require('./baseFlatten'),
    baseIteratee = require('./baseIteratee'),
    restParam = require('../function/restParam');

/**
 * Creates a function like `_.conj`.
 *
 * @private
 * @param {Function} arrayFunc The function to iterate over iteratees.
 * @returns {Function} Returns the new invoker function.
 */
function createInvoker(arrayFunc) {
  return restParam(function(iteratees) {
    iteratees = arrayMap(baseFlatten(iteratees), baseIteratee);
    return restParam(function(args) {
      var thisArg = this;
      return arrayFunc(iteratees, function(iteratee) {
        return iteratee.apply(thisArg, args);
      });
    });
  });
}

module.exports = createInvoker;
