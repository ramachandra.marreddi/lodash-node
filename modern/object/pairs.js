var arrayMap = require('../internal/arrayMap'),
    keys = require('./keys');

/**
 * Creates an array of the key-value pairs for `object`,
 * e.g. `[[key1, value1], [key2, value2]]`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the new array of key-value pairs.
 * @example
 *
 * _.pairs({ 'barney': 36, 'fred': 40 });
 * // => [['barney', 36], ['fred', 40]] (iteration order is not guaranteed)
 */
function pairs(object) {
  return arrayMap(keys(object), function(key) {
    return [key, object[key]];
  });
}

module.exports = pairs;
