var copyObjectWith = require('../internal/copyObjectWith'),
    createAssigner = require('../internal/createAssigner'),
    keys = require('./keys');

/**
 * This method is like `_.assign` except that it accepts `customizer` which
 * is invoked to produce the assigned values. If `customizer` returns `undefined`
 * assignment is handled by the method instead. The `customizer` is invoked
 * with five arguments: (objValue, srcValue, key, object, source).
 *
 * **Note:** This method mutates `object`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The destination object.
 * @param {...Object} sources The source objects.
 * @param {Function} [customizer] The function to customize assigned values.
 * @returns {Object} Returns `object`.
 * @example
 *
 * var defaults = _.partialRight(_.assignWith, function(value, other) {
 *   return _.isUndefined(value) ? other : value;
 * });
 *
 * defaults({ 'user': 'barney' }, { 'age': 36 }, { 'user': 'fred' });
 * // => { 'user': 'barney', 'age': 36 }
 */
var assignWith = createAssigner(function(object, source, customizer) {
  copyObjectWith(source, keys(source), object, customizer);
});

module.exports = assignWith;
