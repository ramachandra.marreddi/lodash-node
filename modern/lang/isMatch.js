var baseIsMatch = require('../internal/baseIsMatch'),
    getMatchData = require('../internal/getMatchData');

/**
 * Performs a deep comparison between `object` and `source` to determine if
 * `object` contains equivalent property values.
 *
 * **Note:** This method supports comparing properties of arrays, booleans,
 * `Date` objects, numbers, `Object` objects, regexes, and strings. Functions
 * and DOM nodes are **not** supported. Provide a customizer function to extend
 * support for comparing other values.
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {Object} object The object to inspect.
 * @param {Object} source The object of property values to match.
 * @returns {boolean} Returns `true` if `object` is a match, else `false`.
 * @example
 *
 * var object = { 'user': 'fred', 'age': 40 };
 *
 * _.isMatch(object, { 'age': 40 });
 * // => true
 *
 * _.isMatch(object, { 'age': 36 });
 * // => false
 */
function isMatch(object, source) {
  return object === source || baseIsMatch(object, source, getMatchData(source));
}

module.exports = isMatch;
