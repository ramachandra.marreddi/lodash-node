module.exports = {
  'add': require('./math/add'),
  'ceil': require('./math/ceil'),
  'floor': require('./math/floor'),
  'max': require('./math/max'),
  'maxBy': require('./math/maxBy'),
  'min': require('./math/min'),
  'minBy': require('./math/minBy'),
  'round': require('./math/round'),
  'sum': require('./math/sum'),
  'sumBy': require('./math/sumBy')
};
